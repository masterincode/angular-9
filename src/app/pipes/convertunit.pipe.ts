import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'convertunit'
})
export class ConvertunitPipe implements PipeTransform {

  transform(value: any, targetUnit: any): any {
    
    switch (targetUnit) {

      case 'cm':   return value * 100;
      case 'km':   return value * 0.001;
      case 'mile': return value * 0.000621371;
      case 'yard': return value * 1.093;
      case 'foot': return value * 3.28;
      case 'inch': return value * 39.370066559999997935;
      default:
        throw new Error('Target Unit Not Supported');
    }
    return value * 1000;
  }

}
