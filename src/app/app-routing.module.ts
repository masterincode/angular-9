import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneratePasswordComponent } from './component/bootcamp/generate-password/generate-password.component';
import { ReactiveFormCodingComponent } from './component/sahosoft/reactive-form-coding/reactive-form-coding.component';
import { ChildCardComponent } from './component/bootcamp/parent-card/child-card/child-card.component';
import { HomePageComponent } from './component/sahosoft/home-page/home-page.component';
import { LoginFormComponent } from './component/sahosoft/login-form/login-form.component';
import { NestedFormArrayComponent } from './component/sahosoft/nested-form-array/nested-form-array.component';
import { SiUnitConverterComponent } from './component/bootcamp/si-unit-converter/si-unit-converter.component';


const routes: Routes = [
  {path:"password-generator",component: GeneratePasswordComponent },
  {path:"signup-form",component: ReactiveFormCodingComponent },
  {path:"card"    ,component: ChildCardComponent },
  {path:"home-page"    ,component: HomePageComponent },
  {path:"login-form"    ,component: LoginFormComponent },
  {path:"nested-form-array"    ,component: NestedFormArrayComponent },
  {path:"si-unit-converter"    ,component: SiUnitConverterComponent },
  {path:"**",component: HomePageComponent }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
