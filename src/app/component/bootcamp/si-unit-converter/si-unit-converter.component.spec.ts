import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiUnitConverterComponent } from './si-unit-converter.component';

describe('SiUnitConverterComponent', () => {
  let component: SiUnitConverterComponent;
  let fixture: ComponentFixture<SiUnitConverterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiUnitConverterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiUnitConverterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
