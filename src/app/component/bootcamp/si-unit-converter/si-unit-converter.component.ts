import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-si-unit-converter',
  templateUrl: './si-unit-converter.component.html',
  styleUrls: ['./si-unit-converter.component.css']
})
export class SiUnitConverterComponent implements OnInit {

  constructor() {}
  ngOnInit(): void {
    this.valueChangeNotifier();
  }

  siunitFormGroup = new FormGroup({
    lenghtMetrics: new FormControl('',Validators.required)
  });
 
  valueChangeNotifier(){
    this.siunitFormGroup.controls['lenghtMetrics'].valueChanges.subscribe(
      (value) => console.log('Metrics=',value)
    );
  }

   
}
