import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-generate-password',
  templateUrl: './generate-password.component.html',
  styleUrls: ['./generate-password.component.css']
})
export class GeneratePasswordComponent implements OnInit {

  constructor() { }

  passwordFormControl = new FormControl('');
  password:String=null;
  includeLetters:boolean = false;
  includeNumbers:boolean = false; 
  includeSymbols: boolean = false;
  length:number=0;

  ngOnInit(): void {
     
  }

  onChangeUseLetters(){
    this.includeLetters = !this.includeLetters;
   }

  onChangeUseNumbers(){
    this.includeNumbers = !this.includeNumbers;
    }

  onChangeUseSymbols(){
    this.includeSymbols = !this.includeSymbols;
  }

  onChangeLength(value : string ) {
    const parsedValue =  parseInt(value);

    if(!isNaN(parsedValue)){
      this.length =parsedValue;
    }
  }

  onButtonClick(){
    // Templete String = `sdasd `
  
               const numbers = '1234567890';
                const letters = 'abcdefghijklmnopqrstuvwyz';
                const symbols = '!@#$%^&*()';
            
                let validChars = '';
                if (this.includeLetters) {
                  validChars += letters;
                }
                if (this.includeNumbers) {
                  validChars += numbers;
                }
                if (this.includeSymbols) {
                  validChars += symbols;
                }
            
                let generatedPassword = '';
                for (let i = 0; i < this.length; i++) {
                  const index = Math.floor(Math.random() * validChars.length);
                  generatedPassword += validChars[index];
                }
                this.password = generatedPassword;

                console.log(`
                Three parameters have values as:
                Letter: ${this.includeLetters}
                Number: ${this.includeNumbers}
                Symbols: ${this.includeSymbols}
                Length : ${this.length} 
                Form Control : ${this.passwordFormControl}
                ` );

  }

}
