import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
  selector: 'app-nested-form-array',
  templateUrl: './nested-form-array.component.html',
  styleUrls: ['./nested-form-array.component.css']
})
export class NestedFormArrayComponent implements OnInit {

  nestedArrayFormGroup : FormGroup;
  constructor() { }
  
  ngOnInit(): void {

    this.nestedArrayFormGroup = new FormGroup({
      mobileNoFormArray: new FormArray([
         new FormControl('7894561235'),
         new FormControl('7898456200')
      ]),
      firstName: new FormControl(''),
      middleName: new FormControl(''),
      lastName: new FormControl('')
    });
  }

  onSubmit() {
    
  }

  

  addNumber():void {
    this.nestedArrayFormGroup.value.mobileNoFormArray.push(new FormControl());
  }

}
