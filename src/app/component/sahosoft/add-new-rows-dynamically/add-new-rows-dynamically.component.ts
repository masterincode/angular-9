import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';

@Component({
  selector: 'app-add-new-rows-dynamically',
  templateUrl: './add-new-rows-dynamically.component.html',
  styleUrls: ['./add-new-rows-dynamically.component.css']
})
export class AddNewRowsDynamicallyComponent implements OnInit {

  constructor(private formBuilder: FormBuilder) { }
  parentformGroup: FormGroup;
  itemRows: FormArray;
  totalRow : number = 0;

  ngOnInit(): void {
    this.parentformGroup = new FormGroup({
        itemRows : new FormArray([this.initialRows()])
    });
    
    
    // this.formBuilder.group({
    //   itemRows: this.formBuilder.array([this.initialRows()])   // This is one Array which has one FormGroup Object inside it
    // });
       
    console.warn(this.parentformGroup.controls.itemRows.value);
  }
  
   initialRows(): FormGroup {
    return this.formBuilder.group({
      firstName : new FormControl(''),
      middleName : new FormControl(''),
      lastName : new FormControl(''),
      mobileNo : new FormControl('')
    });

  }
  
  onSubmit() {
   console.info(this.parentformGroup);
  }

  deleteRow() {
    // const control = <FormArray>this.parentformGroup.controls['itemRows'];
    // if(control != null)
    //   this.totalRow =control.value.length;
    // if(this.totalRow >1 ) {
    //   control.removeAt(index);
    // }
    // else {
    //   alert('One Record is Mandatory');
    //   return false;
    // }
    
  }

  addNewRow():void {
    // const control = <FormArray>this.parentformGroup.controls['itemRows'];
    // control.push(this.initialRows());
  }
}
