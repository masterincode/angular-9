import { Component, OnInit } from '@angular/core';
import { UserProfileFormGroupService } from 'src/app/service/user-profile-form-group.service';
import { FormGroup } from '@angular/forms';
import { UserProfileFormModel } from 'src/app/model/UserProfileFormModel';

@Component({
  selector: 'app-reactive-form-coding',
  templateUrl: './reactive-form-coding.component.html',
  styleUrls: ['./reactive-form-coding.component.css']
})
export class ReactiveFormCodingComponent implements OnInit {

  userProfileForm = new FormGroup({});
  userProfileFormBuilder = new FormGroup({});

  constructor(private userProfileFormGroupService: UserProfileFormGroupService) {
    this.userProfileForm = this.userProfileFormGroupService.userProfileForm;
    // this.userProfileFormBuilder = this.userProfileFormGroupService.userProfileFormBuilder;
    // this.userProfileFormGroupService.formArrayExample();
  }
  
  ngOnInit(): void {
   this.valueChangeSubscribe();
   this.statusChangeSubscribe(); 
   this.userProfileForm.controls['firstName'].setValue('Batista'); 
}

  onSubmit(): void {
    console.warn(this.userProfileForm.value);
    console.log('First Name =', this.userProfileForm.value.firstName);                            // Fetch value Using value =============
    console.log('First Name Using get method=', this.userProfileForm.get('firstName').value);     // Fetch value Using get Method ========
    console.log('First Name Using Control=', this.userProfileForm.controls['firstName'].value);   // Fetch value Using get Controls ======
  }

  resetForm(): void {
    this.userProfileForm.reset();
  }


 private valueChangeSubscribe():void {
    this.userProfileForm.get('firstName').valueChanges.subscribe(         // Using get method ========================
      (value)=>{ console.log('Using get method==',value);
    });
    this.userProfileForm.controls['firstName'].valueChanges.subscribe(   // Using controls ==========================
      (value)=>{ 
        console.log('Using controls==',value);
        // If u want to know how many parameters have been touched after component has been loaded then console "this.userProfileForm.controls" ,
        // go to each variable and check for touched: true , that means it has been touched
        console.warn(this.userProfileForm.controls);
    });
    this.userProfileForm.valueChanges.subscribe(                         // Regular Approach ========================
      (model : UserProfileFormModel)=>{ 
        console.log(`Regular Approach :::::::::::::::::::::::
                     First Name:    ${model.firstName}
                     Last Name:     ${model.lastName}
                     Age:           ${model.age}
                     Email ID:      ${model.email}
                     Technology:    ${model.technology}
                     PersonType:    ${model.persontype}
                     Terms & Cond.. ${model.termsConditions}
        `);
    });
  }

  private statusChangeSubscribe():void {
// statusChange method used to give status of userProfileForm , it is VALID or INVALID

    this.userProfileForm.statusChanges.subscribe(    
      (response)=>{
        console.log('User Profile Form Validation Status==',response);
      }); 
  }

}
