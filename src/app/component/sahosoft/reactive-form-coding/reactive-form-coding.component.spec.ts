import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactiveFormCodingComponent } from './reactive-form-coding.component';

describe('ReactiveFormCodingComponent', () => {
  let component: ReactiveFormCodingComponent;
  let fixture: ComponentFixture<ReactiveFormCodingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactiveFormCodingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactiveFormCodingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
