import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  loginFormGroup = new FormGroup({
    userName : new FormControl('',[Validators.required,Validators.email]),
    password : new FormControl('',[Validators.required]),
    termsConditions : new FormControl('',[Validators.required]),
    loggedIn : new FormControl(''),
  });

  onSubmit() { 
    console.log('loginFormGroup==',this.loginFormGroup.value);
  }

}
