import { TestBed } from '@angular/core/testing';

import { UserProfileFormGroupService } from './user-profile-form-group.service';

describe('UserProfileFormGroupService', () => {
  let service: UserProfileFormGroupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserProfileFormGroupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
